﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour 
{

	public GameObject Focus;
	public Vector3 Offset;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void LateUpdate()
	{
		transform.position = Focus.transform.position + Offset;
	}
}
