﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour 
{
	public float MaxVelocity;
	public float Acceleration;
	public float Braking;
	public float JumpVelocity;
	public float AirControl;

	public float FacingRotation = 140;
	public GameObject VisualCharacter;

	public AudioClip WalkSound;
	public AudioClip JumpSound;

	Rigidbody2D MyRB;
	BoxCollider2D MyCol;
	Animator MyAnimator;
	AudioSource MovementAudio;

	[HideInInspector]
	public bool IsMoving = false;

	public bool IsGrounded = true;

	protected const float StackingRepulsionMag = 500.0f;

	// Use this for initialization
	void Start()
	{
		MyRB = GetComponentInChildren<Rigidbody2D>();
		MyCol = GetComponentInChildren<BoxCollider2D>();
		MyAnimator = GetComponentInChildren<Animator>();
		MovementAudio = gameObject.AddComponent<AudioSource>();
		MovementAudio.clip = WalkSound;

		UpdateGrounded();
	}

	private void Update()
	{
		UpdateAnimator();
	}

	private void FixedUpdate()
	{
		HandleMovement();
	}

	public void Move(Vector2 InputVelocity)
	{
		float VelLimit = Mathf.Abs(InputVelocity.x * MaxVelocity);
		Vector2 AccelVector = InputVelocity.normalized * Acceleration * Time.deltaTime;
		AccelVector = IsGrounded ? AccelVector : AccelVector * AirControl;
	
		Vector2 CurVelocity = MyRB.velocity;

		if (Mathf.Abs(CurVelocity.x + AccelVector.x) >= VelLimit)
		{
			CurVelocity.x = VelLimit * Mathf.Sign(AccelVector.x);
		}
		else
		{
			CurVelocity.x += AccelVector.x;
		}

		// Face direction of movement
		if (VisualCharacter && CurVelocity.magnitude > MaxVelocity * 0.2f)
		{
			Quaternion LocalRot = VisualCharacter.transform.localRotation;
			LocalRot = Quaternion.Euler(LocalRot.eulerAngles.x, FacingRotation * Mathf.Sign(CurVelocity.x), LocalRot.eulerAngles.z);
			VisualCharacter.transform.localRotation = LocalRot;
		}


		CurVelocity.y = MyRB.velocity.y;
		MyRB.velocity = CurVelocity;
		IsMoving = true;

		if(IsGrounded && !MovementAudio.isPlaying)
		{
			MovementAudio.clip = WalkSound;
			MovementAudio.Play();
		}
	}

	public void Jump()
	{
		if (IsGrounded)
		{
			IsGrounded = false;
			MyAnimator.SetTrigger("Jump");
			Vector2 MoveVector = MyRB.velocity;
			MoveVector.y = JumpVelocity;
			MyRB.velocity = MoveVector;

			if (MovementAudio.isPlaying)
				MovementAudio.Stop();

			MovementAudio.clip = JumpSound;
			MovementAudio.Play();
		}
	}

	#region HelperFunctions

	protected void HandleMovement()
	{
		HandleBraking();
		UpdateGrounded();
		IsMoving = false;
	}

	protected void UpdateGrounded()
	{
		LayerMask playerWalkable = LayerMask.GetMask("Terrain");
		Collider2D[] collisions = new Collider2D[32];
		ContactFilter2D filter = new ContactFilter2D();
		filter.SetLayerMask(playerWalkable);
		Vector2 colSize = MyCol.size - Vector2.right * 0.5f;
		Vector2 position2D = transform.position;

		Vector2 tracePoint = (position2D + MyCol.offset) - colSize.y * 0.5f * Vector2.up;

		Vector2 checkSize = colSize;
		checkSize.y = 0.4f;

		int contacts = Physics2D.OverlapBox(tracePoint, checkSize, 0, filter, collisions);
		IsGrounded = contacts > 0 && MyRB.velocity.y <= 0;

		MyAnimator.SetBool("IsGrounded", IsGrounded);
	}

	protected void HandleBraking()
	{
		if (!IsMoving && IsGrounded)
		{
			Vector2 CurVelocity = MyRB.velocity;
			if (CurVelocity.x != 0)
			{
				if (Mathf.Abs(CurVelocity.x) <= Braking * Time.deltaTime)
				{
					CurVelocity.x = 0;
				}
				else
				{
					int Sign = CurVelocity.x < 0 ? 1 : -1;
					CurVelocity.x += Sign * Braking * Time.deltaTime;
				}

				MyRB.velocity = CurVelocity;
			}
		}
	}

	protected void UpdateAnimator()
	{
		MyAnimator.SetFloat("Horizontal", Mathf.Abs(MyRB.velocity.x / MaxVelocity));
	}

#endregion
}
