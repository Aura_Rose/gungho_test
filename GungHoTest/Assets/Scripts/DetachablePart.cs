﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]

public class DetachablePart : MonoBehaviour 
{
	public bool ShouldDestroy;
	protected const float MaxForce = 600.0f;

	Rigidbody2D MyRigidbody;
	BoxCollider2D MyCollider;

	

	// Use this for initialization
	void Start () 
	{
		MyRigidbody = GetComponent<Rigidbody2D>();
		MyCollider = GetComponent<BoxCollider2D>();

		MyRigidbody.simulated = false;
		MyCollider.enabled = false;
	}
	
	public void StartDetachment()
	{
		if(ShouldDestroy)
		{
			Destroy(gameObject);
		}
		else
		{
			MyRigidbody.simulated = true;
			MyCollider.enabled = true;
			transform.SetParent(null);

			MyRigidbody.AddForce(new Vector2(Random.Range(-MaxForce, MaxForce), Random.Range(-MaxForce, MaxForce)));
		}
	}
}
