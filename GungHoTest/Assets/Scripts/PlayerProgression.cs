﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProgression : MonoBehaviour 
{
	public static PlayerProgression PlayerProgressionInst;

	public static float BonusHealth, BonusDamage, BonusForce;
	public static int Scrap;

	protected Character MyCharacter;

	// Use this for initialization
	void Start () 
	{
		PlayerProgressionInst = this;

		BonusHealth = PlayerPrefs.GetFloat("BonusHealth");
		BonusDamage = PlayerPrefs.GetFloat("BonusDamage");
		BonusForce = PlayerPrefs.GetFloat("BonusForce");

		Scrap = PlayerPrefs.GetInt("Scrap");

		MyCharacter = GetComponent<Character>();

		PlayerProgressionInst.MyCharacter.MaxHealth_Bonus = BonusHealth;
		PlayerProgressionInst.MyCharacter.Health = PlayerProgressionInst.MyCharacter.MaxHealth + BonusHealth;
		PlayerProgressionInst.MyCharacter.Damage_Bonus = BonusDamage;
		PlayerProgressionInst.MyCharacter.Impulse_Bonus = BonusForce;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public static void AddScrap(int toAdd)
	{
		Scrap += toAdd;
		SaveData();
	}

	public static void SpendScrap(int toSpend)
	{
		Scrap -= toSpend;
		SaveData();
	}

	public static void IncreaseHealth(float toAdd)
	{
		BonusHealth += toAdd;
		float healthRatio = PlayerProgressionInst.MyCharacter.Health / (PlayerProgressionInst.MyCharacter.MaxHealth + PlayerProgressionInst.MyCharacter.MaxHealth_Bonus);
		PlayerProgressionInst.MyCharacter.Health = healthRatio * (PlayerProgressionInst.MyCharacter.MaxHealth + BonusHealth);
		SaveData();
	}

	public static void IncreaseDamage(float toAdd)
	{
		BonusDamage += toAdd;
		SaveData();
	}

	public static void IncreaseForce(float toAdd)
	{
		BonusForce += toAdd;
		SaveData();
	}

	public static void ResetValues()
	{
		PlayerPrefs.SetFloat("BonusHealth", 0);
		PlayerPrefs.SetFloat("BonusDamage", 0);
		PlayerPrefs.SetFloat("BonusForce", 0);
		PlayerPrefs.SetFloat("Difficulty", 0);
		PlayerPrefs.SetInt("Scrap", 0);
		PlayerPrefs.SetInt("WaveNumber", 0);
	}

	public static void SaveDifficulty(float difficulty)
	{
		PlayerPrefs.SetFloat("Difficulty", difficulty);
	}

	public static void SaveData()
	{
		PlayerProgressionInst.MyCharacter.MaxHealth_Bonus = BonusHealth;
		PlayerProgressionInst.MyCharacter.Damage_Bonus = BonusDamage;
		PlayerProgressionInst.MyCharacter.Impulse_Bonus = BonusForce;

		PlayerPrefs.SetFloat("BonusHealth", BonusHealth);
		PlayerPrefs.SetFloat("BonusDamage", BonusDamage);
		PlayerPrefs.SetFloat("BonusForce", BonusForce);
		PlayerPrefs.SetInt("Scrap", Scrap);
	}
}
