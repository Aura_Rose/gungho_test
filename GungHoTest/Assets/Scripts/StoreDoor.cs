﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreDoor : MonoBehaviour 
{
	public BoxCollider2D MyCollider;
	// Use this for initialization
	void Start () 
	{
		EnemyGenerator.OnWaveDefeated += OnWaveDefeated;
		EnemyGenerator.OnWaveGenerated += OnWaveGenerated;
	}

	private void OnDestroy()
	{
		EnemyGenerator.OnWaveDefeated -= OnWaveDefeated;
		EnemyGenerator.OnWaveGenerated -= OnWaveGenerated;
	}

	protected void OnWaveDefeated()
	{
		MyCollider.enabled = false;
		transform.GetChild(0).gameObject.SetActive(false);
	}

	protected void OnWaveGenerated()
	{
		MyCollider.enabled = true;
		transform.GetChild(0).gameObject.SetActive(true);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		Physics2D.IgnoreCollision(collision, MyCollider);
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		Physics2D.IgnoreCollision(collision, MyCollider, false);
	}
}
