﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour 
{
	Collider2D MyCollider;

	protected virtual void Start()
	{
		MyCollider = GetComponentInChildren<Collider2D>();
	}

	protected void OnTriggerEnter2D(Collider2D collision)
	{
		Physics2D.IgnoreCollision(collision, MyCollider);
	}

	protected void OnTriggerExit2D(Collider2D collision)
	{
		Physics2D.IgnoreCollision(collision, MyCollider, false);
	}
}
