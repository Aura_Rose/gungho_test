﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour 
{
	public enum EActionState
	{
		Idle,
		Cancellable,
		Locked
	};

	// Attributes

	public float MaxHealth;
	public float AttackDamage;
	public float AttackRange;
	public float AttackArc;
	public float AttackImpulse;
	public float AttackSpeed;

	[HideInInspector]
	public float MaxHealth_Bonus;
	[HideInInspector]
	public float Damage_Bonus;
	[HideInInspector]
	public float Impulse_Bonus;

	public int MoneyOnDeath;

	public GameObject OverheadCanvas;

	public AudioClip HitSound;
	public AudioClip DeathSound;

	public ParticleSystem HitFX;
	public GameObject DeathFX;

	CharacterMovement MyCharacterMovement;
	Animator MyAnimator;
	Rigidbody2D MyRigidbody;
	AudioSource CharacterAudio;
	DetachablePart[] MyDetachables;

	EActionState CurActionState;

	[HideInInspector]
	public bool IsAttacking = false;
	[HideInInspector]
	public float Health;
	[HideInInspector]
	public bool IsDead = false;

	protected GameObject CanvasInst;
	

	void Start () 
	{
		MyCharacterMovement = GetComponent<CharacterMovement>();
		MyAnimator = GetComponentInChildren<Animator>();
		MyRigidbody = GetComponent<Rigidbody2D>();
		CharacterAudio = gameObject.AddComponent<AudioSource>();
		MyDetachables = GetComponentsInChildren<DetachablePart>();

		Health = MaxHealth + MaxHealth_Bonus;

		if(OverheadCanvas)
		{
			CanvasInst = Instantiate(OverheadCanvas);
			CanvasInst.GetComponentInChildren<FloatingCanvas>().Focus = gameObject;
		}
	}
	
	void FixedUpdate () 
	{
		if (!IsDead && Health <= 0)
			Die();
	}

	public void Move(Vector2 inputVelocity)
	{
		if(CanMove())
		{
			
			MyCharacterMovement.Move(inputVelocity);
		}
	}

	public void Jump()
	{
		if (CanJump())
		{
			MyCharacterMovement.Jump();
		}
	}

	public void StartAttack()
	{
		if (CanAttack())
		{
			MyAnimator.SetTrigger("Attack");
			MyAnimator.SetFloat("AttackSpeed", AttackSpeed);
			MyAnimator.SetBool("IsAttacking", true);
			CurActionState = EActionState.Locked;
		}
	}

	public void TakeDamage(float damage)
	{
		Health -= damage;
		if(Health <= 0)
		{
			Die();
		}

		if(HitFX)
			HitFX.Play();
	}

	public void ApplyImpulse(Vector2 force)
	{
		MyRigidbody.AddForce(force);
	}

	protected void StopAttack()
	{
		MyAnimator.SetBool("IsAttacking", false);
	}

	protected void Die()
	{
		IsDead = true;

		if(CanvasInst)
			Destroy(CanvasInst);

		MyAnimator.enabled = false;
		MyCharacterMovement.enabled = false;

		if(DeathFX)
		{
			GameObject DeathFX_Inst = Instantiate(DeathFX);
			DeathFX_Inst.transform.position = transform.position;
			DeathFX_Inst.transform.parent = transform;
		}

		gameObject.layer = LayerMask.NameToLayer("FX");

		foreach (DetachablePart part in MyDetachables)
		{
			part.StartDetachment();
		}

		StartCoroutine("DeathCleanup");

		PlaySound(DeathSound);

		if(!gameObject.CompareTag("Player") && EnemyGenerator.CurrentWave.Contains(gameObject))
		{
			EnemyGenerator.CurrentWave.Remove(gameObject);
			PlayerProgression.AddScrap(MoneyOnDeath);
		}
	}

	IEnumerator DeathCleanup()
	{
		yield return new WaitForSeconds(1.5f);

		if (gameObject.CompareTag("Player"))
		{
			GameMenu.GameMenuInst.OpenDeathMenu();
			PlayerProgression.ResetValues();
		}
		else
		{
			yield return new WaitForSeconds(3.5f);
			for (int i = MyDetachables.Length - 1; i >=0; i--)
			{
				DetachablePart part = MyDetachables[i];
				if(part)
					Destroy(part.gameObject);
			}

			Destroy(gameObject);
		}
	}

	#region ActionState

	public bool CanMove()
	{
		return !IsDead && CurActionState != EActionState.Locked;
	}

	public bool CanJump()
	{
		return MyCharacterMovement.IsGrounded && !IsDead && CurActionState != EActionState.Locked;
	}

	public bool CanAttack()
	{
		return !IsDead && CurActionState == EActionState.Idle;
	}

	public void AnimTrigger_AttackCancel()
	{
		CurActionState = EActionState.Cancellable;
	}

	public void AnimTrigger_AttackEnd()
	{
		CurActionState = EActionState.Idle;
		StopAttack();
	}

	public void AnimTrigger_AttackDamage()
	{
		if (IsDead)
			return;

		int characterLayer = LayerMask.GetMask("Character");
		Collider2D[] collidersHit = Physics2D.OverlapCircleAll(transform.position, AttackRange, characterLayer);

		foreach(Collider2D collider in collidersHit)
		{
			Vector2 toOther = collider.transform.position - transform.position;

			if (Vector2.Angle(transform.forward, toOther) > AttackArc)
				continue;

			if(collider.gameObject != gameObject)
			{
				Character otherCharacter = collider.GetComponent<Character>();
	
				if(otherCharacter)
				{
					PlaySound(HitSound);
					otherCharacter.TakeDamage(AttackDamage + Damage_Bonus);
					otherCharacter.ApplyImpulse(toOther * (AttackImpulse + Impulse_Bonus));
				}
			}
		}
	}

	#endregion

	protected void PlaySound(AudioClip sound)
	{
		CharacterAudio.clip = sound;
		CharacterAudio.Play();
	}
}
