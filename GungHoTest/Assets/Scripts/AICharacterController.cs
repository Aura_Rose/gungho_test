﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICharacterController : MonoBehaviour 
{
	public float AlarmDistance;
	public float ReactTimeMin;
	public float ReactTimeMax;
	
	protected GameObject PlayerObject;
	protected Character MyCharacter;

	protected const float AttackChance = 0.85f;
	protected const float ChoiceInterval = 1.0f;
	protected const float EvadeJumpChance = 0.05f;

	protected bool IsDelayingAttack;
	protected float LastChooseTime = 0.0f;

	protected delegate void DoAction();
	protected DoAction Act;

	// Use this for initialization
	void Start () 
	{
		PlayerObject = GameObject.FindGameObjectWithTag("Player");
		MyCharacter = GetComponent<Character>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.time >= LastChooseTime + ChoiceInterval)
		{
			LastChooseTime = Time.time;

			if (Vector2.Distance(PlayerObject.transform.position, transform.position) < AlarmDistance)
			{
				if (Random.value <= AttackChance)
					Act = Attack;
				else
					Act = Evade;
			}
			else
			{
				Act = Wander;
			}
		}

		if(Act != null)
			Act();
	}

	protected void Wander()
	{ 
		
	}


	protected void Attack()
	{
		Vector2 toPlayer = (PlayerObject.transform.position - transform.position).normalized;
		float angleToPlayer = Vector2.Angle(transform.forward, toPlayer);
		if (Vector2.Distance(PlayerObject.transform.position, transform.position) < MyCharacter.AttackRange && angleToPlayer < 90)
		{
			if(!IsDelayingAttack)
				StartCoroutine("DelayedAttack");
		}
		else
		{
			MoveToTarget();
		}
	}

	protected void Evade()
	{
		Vector2 toPlayer = (PlayerObject.transform.position - transform.position).normalized;
		Vector2 MoveVector = new Vector2(Mathf.Sign(-toPlayer.x), 0);
		MyCharacter.Move(MoveVector);
		if(Random.value > EvadeJumpChance)
			MyCharacter.Jump();
	}

	IEnumerator DelayedAttack()
	{
		IsDelayingAttack = true;
		yield return new WaitForSeconds(Random.Range(ReactTimeMin, ReactTimeMax));
		MyCharacter.StartAttack();
		IsDelayingAttack = false;
	}

	// TODO: pathfinding
	protected void MoveToTarget()
	{
		Vector2 toPlayer = (PlayerObject.transform.position - transform.position).normalized;
		Vector2 MoveVector = new Vector2(Mathf.Sign(toPlayer.x), 0);
		MyCharacter.Move(MoveVector);
		if(toPlayer.y > transform.position.y + 1.0f)
			MyCharacter.Jump();
	}
}
