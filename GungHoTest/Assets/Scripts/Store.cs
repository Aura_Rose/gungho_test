﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Store : MonoBehaviour 
{
	public GameObject StoreScreen;
	public Text RepairCost_Text;
	public Text HealthCost_Text;
	public Text DamageCost_Text;
	public Text ForceCost_Text;
	public Text Health_Text;
	public Text Damage_Text;
	public Text Force_Text;
	
	protected Character PlayerCharacter;

	protected const int RepairPerUse = 25;
	protected const float HealthPerLevel = 20;
	protected const float DamagePerLevel = 5;
	protected const float ForcePerLevel = 40;

	protected const int RepairCost = 5;
	protected const int HealthCost = 40;
	protected const int DamageCost = 40;
	protected const int ForceCost = 20;

	protected Button FirstSelected;

	// Use this for initialization
	void Start () 
	{
		PlayerCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
		FirstSelected = StoreScreen.GetComponentInChildren<Button>();
		CloseStore();
	}
	
	// Update is called once per frame
	void Update () 
	{
		RepairCost_Text.text = RepairCost.ToString();
		HealthCost_Text.text = HealthCost.ToString();
		DamageCost_Text.text = DamageCost.ToString();
		ForceCost_Text.text = ForceCost.ToString();
		Health_Text.text = "Health: " + (PlayerCharacter.MaxHealth + PlayerCharacter.MaxHealth_Bonus);
		Damage_Text.text = "Damage: " + (PlayerCharacter.AttackDamage + PlayerCharacter.Damage_Bonus);
		Force_Text.text = "Force: " + (PlayerCharacter.AttackImpulse + PlayerCharacter.Impulse_Bonus);
	}

	public void Repair()
	{
		if(PlayerProgression.Scrap < RepairCost)
		{
			// Error: not enough scrap
			return;
		}

		if(PlayerCharacter.Health == (PlayerCharacter.MaxHealth + PlayerCharacter.MaxHealth_Bonus))
		{
			// Error: repair unnecessary
			return;
		}
		PlayerProgression.SpendScrap(RepairCost);
		PlayerCharacter.Health = Mathf.Min(PlayerCharacter.Health + RepairPerUse, (PlayerCharacter.MaxHealth + PlayerCharacter.MaxHealth_Bonus));
	}

	public void UpgradeHealth()
	{
		if (PlayerProgression.Scrap >= HealthCost)
		{
			PlayerProgression.SpendScrap(HealthCost);
			PlayerProgression.IncreaseHealth(HealthPerLevel);
		}
	}

	public void UpgradeDamage()
	{
		if (PlayerProgression.Scrap >= DamageCost)
		{
			PlayerProgression.SpendScrap(DamageCost);
			PlayerProgression.IncreaseDamage(DamagePerLevel);
		}
	}

	public void UpgradeForce()
	{
		if (PlayerProgression.Scrap >= ForceCost)
		{
			PlayerProgression.SpendScrap(ForceCost);
			PlayerProgression.IncreaseForce(ForcePerLevel);
		}
	}

	public void CloseStore()
	{
		PlayerInput.IsUIOpen = false;
		StoreScreen.SetActive(false);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.gameObject.CompareTag("Player") && !PlayerInput.IsUIOpen)
		{
			StoreScreen.SetActive(true);
			FirstSelected.Select();
			PlayerInput.IsUIOpen = true;
		}
	}
}
