﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
	public static bool IsUIOpen = false;

	Character MyCharacter;

	private void Start()
	{
		MyCharacter = GetComponent<Character>();
	}

	private void Update()
	{
		ProcessInput();
	}

	protected void ProcessInput()
	{
		bool openMenu = Input.GetButtonDown("OpenMenu");

		if (openMenu)
		{
			if (GameMenu.GameMenuInst.IsMenuOpen())
				GameMenu.GameMenuInst.ClosePauseMenu();
			else
				GameMenu.GameMenuInst.OpenPauseMenu();
		}

		if (IsUIOpen)
			return;

		float horInput = Input.GetAxis("Horizontal");
		bool jumpInput = Input.GetButtonDown("Jump");
		bool attackInput = Input.GetAxis("Attack") > 0;

		if (horInput != 0)
		{
			Vector2 MoveVector = Vector2.right * Mathf.Sign(horInput);

			MyCharacter.Move(MoveVector);
		}

		if (jumpInput)
		{
			if (MyCharacter.CanJump())
			{
				
				MyCharacter.Jump();
			}	
	
		}
		else if (attackInput)
		{
			MyCharacter.StartAttack();
		}
	}
}
