﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameMenu : MonoBehaviour 
{
	public static GameMenu GameMenuInst;

	public GameObject MenuObject;
	public GameObject DeathMenuObject;

	protected Button MenuFirstButton;
	protected Button DeathMenuFirstButton;

	protected bool IsDeathMenuOpen;

	// Use this for initialization
	void Start () 
	{
		GameMenuInst = this;
		MenuFirstButton = MenuObject.GetComponentInChildren<Button>();
		DeathMenuFirstButton = DeathMenuObject.GetComponentInChildren<Button>();

		ClosePauseMenu();
	}

	public bool IsMenuOpen()
	{
		return MenuObject.activeSelf;
	}

	public void OpenPauseMenu()
	{
		if(!IsDeathMenuOpen)
		{
			PlayerInput.IsUIOpen = true;
			MenuObject.SetActive(true);

			EventSystem.current.SetSelectedGameObject(null);
			MenuFirstButton.Select();
			
		}
		
		//Time.timeScale = 0;
	}

	public void ClosePauseMenu()
	{
		PlayerInput.IsUIOpen = false;
		MenuObject.SetActive(false);
		//Time.timeScale = 1;
	}

	public void OpenDeathMenu()
	{
		PlayerInput.IsUIOpen = true;
		DeathMenuObject.SetActive(true);

		IsDeathMenuOpen = true;
		ClosePauseMenu();

		EventSystem.current.SetSelectedGameObject(null);
		DeathMenuFirstButton.Select();	
	}

	public void RestartGame()
	{
		PlayerProgression.ResetValues();
		SceneManager.LoadSceneAsync("GameLevel");
	}

	public void MainMenu()
	{
		SceneManager.LoadSceneAsync("MainMenu");
	}

	public void ExitGame()
	{
		Application.Quit();
	}
}
