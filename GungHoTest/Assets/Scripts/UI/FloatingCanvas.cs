﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingCanvas : MonoBehaviour 
{
	public GameObject Focus;

	protected Vector3 Offset;

	// Use this for initialization
	protected virtual void Start () 
	{
		Offset = transform.position;
	}

	// Update is called once per frame
	protected virtual void LateUpdate () 
	{
		if(!Focus)
		{
			Destroy(gameObject);
		}
		else
		{
			transform.position = Focus.transform.position + Offset;
		}
	}
}
