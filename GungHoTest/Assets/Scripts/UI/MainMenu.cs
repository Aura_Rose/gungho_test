﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	public GameObject MainCanvas;
	public GameObject CustomizeCanvas;

	public Slider MainSlider_R;
	public Slider MainSlider_G;
	public Slider MainSlider_B;

	public Slider OutlineSlider_R;
	public Slider OutlineSlider_G;
	public Slider OutlineSlider_B;

	public Slider OutlineSlider_Thickness;

	public Renderer CharacterRenderer;

	protected Color MainColor;
	protected Color OutlineColor;
	protected float OutlineSize;

	protected Material MainMaterial;

	protected Button MainFirstButton;
	protected Button CustomizeFirstButton;

	// Use this for initialization
	void Start () 
	{
		MainMaterial = CharacterRenderer.sharedMaterials[1];
		LoadSettings();

		MainMaterial.EnableKeyword("_OutlineColor");
		MainMaterial.EnableKeyword("_Thickness");

		MainFirstButton = MainCanvas.GetComponentInChildren<Button>();
		CustomizeFirstButton = CustomizeCanvas.GetComponentInChildren<Button>();
	}

	protected void LoadSettings()
	{
		MainColor = new Color(PlayerPrefs.GetFloat("MainColor_R"), PlayerPrefs.GetFloat("MainColor_G"), PlayerPrefs.GetFloat("MainColor_B"));
		OutlineColor = new Color(PlayerPrefs.GetFloat("OutlineColor_R"), PlayerPrefs.GetFloat("OutlineColor_G"), PlayerPrefs.GetFloat("OutlineColor_B"));
		OutlineSize = PlayerPrefs.GetFloat("OutlineSize");

		if(OutlineSize == 0)
			OutlineSize = 0.025f;
		
		MainMaterial.SetColor(Shader.PropertyToID("_Color"), MainColor);
		MainMaterial.SetColor(Shader.PropertyToID("_OutlineColor"), OutlineColor);
		MainMaterial.SetFloat(Shader.PropertyToID("_Thickness"), OutlineSize);

		MainSlider_R.value = MainColor.r;
		MainSlider_G.value = MainColor.g;
		MainSlider_B.value = MainColor.b;
		OutlineSlider_R.value = OutlineColor.r;
		OutlineSlider_G.value = OutlineColor.g;
		OutlineSlider_B.value = OutlineColor.b;
		OutlineSlider_Thickness.value = OutlineSize;
	}

#region MainMenu

	public void StartGame()
	{
		SceneManager.LoadSceneAsync("GameLevel");
	}

	public void Customize()
	{
		MainCanvas.SetActive(false);
		CustomizeCanvas.SetActive(true);
		CustomizeFirstButton.Select();
	}

	public void ExitGame()
	{
		Application.Quit();
	}
#endregion

#region CustomizeMenu

	public void SetMainRed(float val)
	{
		if(MainMaterial)
		{
			MainColor.r = val;
			MainMaterial.SetColor("_Color", MainColor);
		}
	}

	public void SetMainGreen(float val)
	{
		if (MainMaterial)
		{
			MainColor.g = val;
			MainMaterial.SetColor("_Color", MainColor);
		}
	}

	public void SetMainBlue(float val)
	{
		if (MainMaterial)
		{
			MainColor.b = val;
			MainMaterial.SetColor("_Color", MainColor);
		}
	}

	public void SetOutlineRed(float val)
	{
		if (MainMaterial)
		{
			OutlineColor.r = val;
			MainMaterial.SetColor("_OutlineColor", OutlineColor);
		}
	}

	public void SetOutlineGreen(float val)
	{
		if (MainMaterial)
		{
			OutlineColor.g = val;
			MainMaterial.SetColor("_OutlineColor", OutlineColor);
		}
	}

	public void SetOutlineBlue(float val)
	{
		if (MainMaterial)
		{
			OutlineColor.b = val;
			MainMaterial.SetColor("_OutlineColor", OutlineColor);
		}
	}

	public void SetOutlineThickness(float val)
	{
		if (MainMaterial)
		{
			OutlineSize = val;
			MainMaterial.SetFloat("_Thickness", OutlineSize);
		}
	}

	public void SaveSettings()
	{
		PlayerPrefs.SetFloat("MainColor_R", MainColor.r);
		PlayerPrefs.SetFloat("MainColor_G", MainColor.g);
		PlayerPrefs.SetFloat("MainColor_B", MainColor.b);
		PlayerPrefs.SetFloat("OutlineColor_R",	OutlineColor.r);
		PlayerPrefs.SetFloat("OutlineColor_G",	OutlineColor.g);
		PlayerPrefs.SetFloat("OutlineColor_B",	OutlineColor.b);
		PlayerPrefs.SetFloat("OutlineSize", OutlineSize);

		MainCanvas.SetActive(true);
		CustomizeCanvas.SetActive(false);
		MainFirstButton.Select();
	}

	public void CancelSettings()
	{
		LoadSettings();

		MainCanvas.SetActive(true);
		CustomizeCanvas.SetActive(false);
		MainFirstButton.Select();
	}

	#endregion

}
