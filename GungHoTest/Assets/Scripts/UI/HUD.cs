﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour 
{
	public Text HealthText;
	public Slider HealthSlider;
	public Text ScrapText;
	public Text StateText;
	public Text StateNumberText;

	protected Character PlayerCharacter;

	private void Start()
	{
		PlayerCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
	}

	void Update () 
	{
		HealthText.text = PlayerCharacter.Health + " / " + (PlayerCharacter.MaxHealth + PlayerCharacter.MaxHealth_Bonus);
		HealthSlider.value = PlayerCharacter.Health;
		HealthSlider.maxValue = PlayerCharacter.MaxHealth + PlayerCharacter.MaxHealth_Bonus;
		ScrapText.text = PlayerProgression.Scrap.ToString();

		if(EnemyGenerator.TimeUntilNextWave > 0)
		{
			StateText.text = "Time Until Next Wave: ";
			StateNumberText.text = EnemyGenerator.TimeUntilNextWave.ToString("0.00");
		}
		else
		{
			StateText.text = "Wave " + (EnemyGenerator.WaveNumber + 1);
			StateNumberText.text = "Enemies Left: " + EnemyGenerator.CurrentWave.Count;
		}
	}
}
