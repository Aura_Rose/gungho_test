﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingEnemyUI : FloatingCanvas
{
	public Slider HealthBar;

	protected Character OwningCharacter;

	// Use this for initialization
	protected override void Start() 
	{
		base.Start();
		OwningCharacter = Focus.GetComponent<Character>();
	}

	// Update is called once per frame
	protected override void LateUpdate() 
	{
		base.LateUpdate();

		if (HealthBar)
		{
			HealthBar.value = OwningCharacter.Health;
			HealthBar.maxValue = OwningCharacter.MaxHealth + OwningCharacter.MaxHealth_Bonus;
		}
	}
}
