﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour 
{
	// EnemyPrefab, and DifficultyLevel to spawn at
	[System.Serializable]
	public struct SpawnEntry
	{
		public GameObject Prefab;
		public float Difficulty;
	}

	public SpawnEntry[] EnemyPrefabs; 
	public GameObject[] SpawnPoints;
	public float StartingDifficulty;
	public float DifficultyPerWave;
	public float TimeBetweenWaves;

	public static float TimeUntilNextWave;
	public static int WaveNumber;

	[HideInInspector]
	public float DifficultyLevel;

	public static List<GameObject> CurrentWave;
	protected bool bWaveDefeated = false;

	// Use this for initialization
	void Start () 
	{
		CurrentWave = new List<GameObject>();
		DifficultyLevel = PlayerPrefs.GetFloat("Difficulty");

		if(DifficultyLevel == 0)
			DifficultyLevel = StartingDifficulty;

		WaveNumber = PlayerPrefs.GetInt("WaveNumber");

		EnemyPrefabs.OrderBy(x => x.Difficulty);
		GenerateWave();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!bWaveDefeated && CurrentWave.Count == 0)
		{
			StartCoroutine("WaveDefeated");
		}

		if(TimeUntilNextWave > 0)
		{
			TimeUntilNextWave = Mathf.Max(TimeUntilNextWave - Time.deltaTime, 0);
		}
	}

	public delegate void EventWaveDefeated();
	public static EventWaveDefeated OnWaveDefeated;

	IEnumerator WaveDefeated()
	{
		bWaveDefeated = true;
		WaveNumber++;
		PlayerPrefs.SetInt("WaveNumber", WaveNumber);
		if (OnWaveDefeated != null)
			OnWaveDefeated();

		TimeUntilNextWave = TimeBetweenWaves;
		yield return new WaitForSeconds(TimeBetweenWaves);

		DifficultyLevel += WaveNumber * DifficultyPerWave;
		PlayerProgression.SaveDifficulty(DifficultyLevel);
		GenerateWave();
		bWaveDefeated = false;
	}

	public delegate void EventWaveGenerated();
	public static EventWaveGenerated OnWaveGenerated;

	void GenerateWave()
	{
		float difficultyBudget = DifficultyLevel;

		while(difficultyBudget > 0)
		{
			int index = Random.Range(0, EnemyPrefabs.Length);

			while (difficultyBudget < EnemyPrefabs[index].Difficulty && index >= 0) 
			{
				index--;
			}
			if (index < 0)
				break;

			difficultyBudget -= EnemyPrefabs[index].Difficulty;
			Vector2 spawnPosition= SpawnPoints[Random.Range(0, SpawnPoints.Length)].transform.position;
			GameObject spawnedEnemy = Instantiate(EnemyPrefabs[index].Prefab);
			spawnedEnemy.transform.position = spawnPosition;
			CurrentWave.Add(spawnedEnemy);
		}

		if(OnWaveGenerated != null)
			OnWaveGenerated();
	}
}
