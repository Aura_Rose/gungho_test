# README #

### Controls ###

Mouse - Left click to attack
Keyboard - WASD to move, space to jump, CTRL to attack, ESC to open options menu in game
Controller - Left thumbstick or DPad to move, A to jump, X to attack, left menu button to open options menu in game

To use the store, move over the pad with the store label over it.

### Gameplay ###

This game is an endless wave based arena game. The waves get increasingly difficult as you clear them, but you can keep up with this by spending scrap to upgrade and repair your robot in the shop.
In the main menu, the Customize screen will let you choose color and outline settings for your robot. 
